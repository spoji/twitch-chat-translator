var counter=0;

$(document).arrive(".message", function() {
    var message = $(this).text();
    if(message.indexOf("д") != -1 
    || message.indexOf("в") != -1 
    || message.indexOf("г") != -1 
    || message.indexOf("н") != -1 
    || message.indexOf("т") != -1 
    || message.indexOf("ж") != -1 
    || message.indexOf("к") != -1 
    || message.indexOf("и") != -1 
    || message.indexOf("л") != -1 
    || message.indexOf("ч") != -1 
    || message.indexOf("б") != -1 
    || message.indexOf("ы") != -1 
    || message.indexOf("я") != -1 
    || message.indexOf("ф") != -1){
        $(this).attr("id", "translation" + counter);
        chrome.runtime.sendMessage({
            from:    "content",
            subject: "translate",
            message: message,
            idcounter: counter
        });
        $(this).text($(this).text() + " TRANSLATING...");
        counter++;
    }
});

chrome.runtime.onMessage.addListener(function (msg, sender, response){
    if ((msg.from === 'popup') && (msg.subject === 'translation')){
        var domItem = $("#translation"+msg.idcounter);
        domItem.attr("tooltiptext", domItem.text().substr(-15));
        domItem.text(msg.translation + " (translated).");
    }
});