chrome.runtime.onMessage.addListener(function (msg, sender) {
    if ((msg.from === "content") && (msg.subject === "translate")) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "http://mymemory.translated.net/api/get?q=" + encodeURIComponent(msg.message) + "&langpair=ru|en", true);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                var response = JSON.parse(xhr.responseText);
                chrome.tabs.sendMessage(
                    sender.tab.id,
                    {
                        from: 'popup', 
                        subject: 'translation', 
                        idcounter: msg.idcounter,
                        translation: response.responseData.translatedText
                    }
                );
            }
        }
        xhr.send();
    }
});